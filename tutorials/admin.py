from django.contrib import admin
from .models import Lesson, Category, Like, Comment, UserLessonClass, UserCategoryClass


@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    list_display = ['id',
                    "title",
                    "slug",
                    "created",
                    ]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id',
                    "title",
                    "slug",
                    "created",
                    ]


# prepopulated_fields = {'slug': ('title',)}
@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['id',
                    "author",
                    "num_like",
                    "created",
                    ]


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = ['id',
                    "user",
                    ]


@admin.register(UserLessonClass)
class UserLessonClassAdmin(admin.ModelAdmin):
    list_display = ['id',
                    "user",
                    "lesson",
                    "join_at",
                    "grade"
                    ]


@admin.register(UserCategoryClass)
class UserCategoryClassAdmin(admin.ModelAdmin):
    list_display = ['id',
                    "user",
                    "category",
                    "complete_category",
                    ]
