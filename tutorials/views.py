from django.shortcuts import render, get_object_or_404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import viewsets, filters, status
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import (LessonSerializer, CategorySerializer,
                          LikeSerializer, LikedObjectRelatedField,
                          CommentSerializer, CommentingSerializer,
                          LessonNameListSerializer, CategoryComplete)

from tutorials.models import Lesson, Category, Like, Comment, UserLessonClass, UserCategoryClass


# AllowAny
# IsAuthenticated
# IsAdminUser
# IsAuthenticatedOrReadOnly

class LikeViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    http_method_names = ['get']
    filter_backends = [filters.SearchFilter]


class LessonViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer
    http_method_names = ['get']
    filter_backends = [filters.SearchFilter]
    search_fields = ['title', 'slug']

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.view_count += 1
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class CategoryViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    http_method_names = ['get']
    filter_backends = [filters.SearchFilter]
    search_fields = ['title', 'slug']

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.view_count += 1
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


# class LikeLessonView(APIView):
#     permission_classes = (IsAuthenticated,)
#
#     def put(self, request, format=None, pk=0):
#         user = request.user
#         lesson = get_object_or_404(Lesson, id=pk)
#
#         if user.profiles not in lesson.like.all():
#             lesson.like.add(user.profiles)
#             lesson.save()
#             return Response('liked successfully', status=status.HTTP_400_BAD_REQUEST)
#         else:
#             return Response('already liked', status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, format=None, pk=0):
#         user = request.user
#         lesson = get_object_or_404(Lesson, id=pk)
#         if user.profiles in lesson.like.all():
#             lesson.like.remove(user.profiles)
#             lesson.save()
#             return Response('unliked successfully', status=status.HTTP_400_BAD_REQUEST)
#         else:
#             return Response('already unliked', status=status.HTTP_400_BAD_REQUEST)


class LikeLessonView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, format=None, pk=0):
        user = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        try:
            lesson.likes.get(user=user)
            return Response('already liked', status=status.HTTP_400_BAD_REQUEST)
        except Like.DoesNotExist:
            lesson.likes.create(user=user)
            lesson.num_like += 1
            lesson.save()
            return Response('liked successfully', status=status.HTTP_200_OK)

    def delete(self, request, format=None, pk=0):
        user = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        try:
            lesson.likes.get(user=user).delete()
            lesson.num_like -= 1
            lesson.save()
            return Response('unliked successfully', status=status.HTTP_200_OK)
        except Like.DoesNotExist:
            return Response('already unliked', status=status.HTTP_400_BAD_REQUEST)


class LikeCategoryView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, format=None, pk=0):
        user = request.user
        category = get_object_or_404(Category, id=pk)
        try:
            category.likes.get(user=user)
            return Response('already liked', status=status.HTTP_400_BAD_REQUEST)
        except Like.DoesNotExist:
            category.likes.create(user=user)
            category.num_like += 1
            category.save()
            return Response('liked successfully', status=status.HTTP_200_OK)

    def delete(self, request, format=None, pk=0):
        user = request.user
        category = get_object_or_404(Category, id=pk)
        try:
            category.likes.get(user=user).delete()
            category.num_like -= 1
            category.save()
            return Response('unliked successfully', status=status.HTTP_200_OK)
        except Like.DoesNotExist:
            return Response('already unliked', status=status.HTTP_400_BAD_REQUEST)


class LikeCommentLessonView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, format=None, pk=0, pk_c=0):
        global comment
        user = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        try:
            comment = lesson.comment.get(pk=pk_c)
            comment.likes.get(user=user)
            return Response('already liked', status=status.HTTP_400_BAD_REQUEST)
        except Comment.DoesNotExist:
            return Response('Comment not found', status=status.HTTP_404_NOT_FOUND)
        except Like.DoesNotExist:
            comment.likes.create(user=user)
            comment.num_like += 1
            comment.save()
            return Response('liked successfully', status=status.HTTP_200_OK)

    def delete(self, request, format=None, pk=0, pk_c=0):
        user = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        try:
            comment = lesson.comment.get(id=pk_c)
            comment.likes.get(user=user).delete()
            comment.num_like -= 1
            comment.save()
            return Response('unliked successfully', status=status.HTTP_200_OK)
        except Comment.DoesNotExist:
            return Response('Comment not found', status=status.HTTP_404_NOT_FOUND)
        except Like.DoesNotExist:
            return Response('already unliked', status=status.HTTP_400_BAD_REQUEST)


class LikeCommentCategoryView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, format=None, pk=0, pk_c=0):
        global comment
        user = request.user
        category = get_object_or_404(Category, id=pk)
        try:
            comment = category.comment.get(pk=pk_c)
            comment.likes.get(user=user)
            return Response('already liked', status=status.HTTP_400_BAD_REQUEST)
        except Comment.DoesNotExist:
            return Response('Comment not found', status=status.HTTP_404_NOT_FOUND)
        except Like.DoesNotExist:
            comment.likes.create(user=user)
            comment.num_like += 1
            comment.save()
            return Response('liked successfully', status=status.HTTP_200_OK)

    def delete(self, request, format=None, pk=0, pk_c=0):
        user = request.user
        category = get_object_or_404(Category, id=pk)
        try:
            comment = category.comment.get(id=pk_c)
            comment.likes.get(user=user).delete()
            comment.num_like -= 1
            comment.save()
            return Response('unliked successfully', status=status.HTTP_200_OK)
        except Comment.DoesNotExist:
            return Response('Comment not found', status=status.HTTP_404_NOT_FOUND)
        except Like.DoesNotExist:
            return Response('already unliked', status=status.HTTP_400_BAD_REQUEST)


class CommentCategoryView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None, pk=0):
        author = request.user
        category = get_object_or_404(Category, id=pk)
        serializer = CommentingSerializer(data=request.data)
        if serializer.is_valid():
            category.comment.create(author=author, text=serializer.validated_data['text'])
            return Response('Comment successfully', status=status.HTTP_200_OK)
        return Response({'message': 'validation failed',
                         'errors': serializer.errors}, status=status.HTTP_404_NOT_FOUND)


class CommentLessonView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None, pk=0):
        author = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        serializer = CommentingSerializer(data=request.data)
        if serializer.is_valid():
            lesson.comment.create(author=author, text=serializer.validated_data['text'])
            return Response('Comment successfully', status=status.HTTP_200_OK)
        return Response({'message': 'validation failed',
                         'errors': serializer.errors}, status=status.HTTP_404_NOT_FOUND)


class LessonEnrollView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk=0, format=None):
        user = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        try:
            UserLessonClass.objects.get(user=user, lesson=lesson)
            return Response('you have already enrolled', status=status.HTTP_400_BAD_REQUEST)
        except UserLessonClass.DoesNotExist:
            UserLessonClass.objects.create(user=user, lesson=lesson)
            return Response('you have successfully enrolled', status=status.HTTP_201_CREATED)


class LessonDisEnrollView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, pk=0, format=None):
        user = request.user
        lesson = get_object_or_404(Lesson, id=pk)
        try:
            UserLessonClass.objects.get(user=user, lesson=lesson).delete()
            return Response('Successfully disenroll', status=status.HTTP_200_OK)
        except UserLessonClass.DoesNotExist:
            return Response('Disenroll already', status=status.HTTP_404_NOT_FOUND)


class UserEnrollListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        user = request.user
        x = UserLessonClass.objects.filter(user=user)
        enroll_list = LessonNameListSerializer(x, many=True)
        if enroll_list.data:
            return Response(enroll_list.data, status=status.HTTP_200_OK)
        else:
            return Response("There is no lesson you have enrolled", status=status.HTTP_404_NOT_FOUND)


class CompleteCategoryView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request, pk=0, format=None):
        serializer = CategoryComplete(data=request.data)
        if serializer.is_valid():
            user = request.user
            bool_ = serializer.validated_data['complete_category']
            category = Category.objects.get(id=pk)
            lesson = Lesson.objects.get(category=category)
            try:
                UserLessonClass.objects.get(lesson=lesson, user=user)
                user_category = UserCategoryClass.objects.get(user=user, category=category)
                user_category.complete_category = bool_
                user_category.save()
                return Response("Successfully change status", status=status.HTTP_200_OK)
            except UserCategoryClass.DoesNotExist:
                user_category = UserCategoryClass.objects.create(user=user, category=category)
                user_category.complete_category = bool_
                user_category.save()
                return Response("Successfully change status", status=status.HTTP_200_OK)
            except UserLessonClass.DoesNotExist:
                return Response("You haven't enrolled the lesson", status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({'message': 'validation failed',
                             'errors': serializer.errors}, status=status.HTTP_404_NOT_FOUND)
