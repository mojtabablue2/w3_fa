from .views import (LessonViewSet, CategoryViewSet,
                    LikeLessonView, LikeCategoryView,
                    LikeViewSet, CommentCategoryView,
                    CommentLessonView, LikeCommentCategoryView,
                    LikeCommentLessonView, LessonEnrollView,
                    UserEnrollListView, LessonDisEnrollView,
                    CompleteCategoryView,)
from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()

router.register('lessons', LessonViewSet)
router.register('category', CategoryViewSet)
router.register('like', LikeViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('lessons/<int:pk>/like/', LikeLessonView.as_view(), name='like_lesson'),
    path('category/<int:pk>/like/', LikeCategoryView.as_view(), name='like_category'),
    path('lessons/<int:pk>/comment/', CommentLessonView.as_view(), name='comment_lesson'),
    path('category/<int:pk>/comment/', CommentCategoryView.as_view(), name='comment_category'),
    path('lessons/<int:pk>/comment/<int:pk_c>/like/', LikeCommentLessonView.as_view(), name='comment_lesson_like'),
    path('category/<int:pk>/comment/<int:pk_c>/like/', LikeCommentCategoryView.as_view(), name='comment_category_like'),
    path('lessons/<int:pk>/enroll/', LessonEnrollView.as_view(), name='enroll_lesson'),
    path('lessons/<int:pk>/disenroll/', LessonDisEnrollView.as_view(), name='enroll_lesson'),
    path('enroll_list/', UserEnrollListView.as_view(), name='user_enroll_list'),
    path('category/<int:pk>/complete/', CompleteCategoryView.as_view(), name='complete_category'),

    # path('', LessonListView.as_view(), name='lessons'),
    # path('<int:pk>/', LessonInfoView.as_view(), name='lessons_detail'),
    # path('category/', CategoryListView.as_view(), name='categories'),
    # path('category/<int:pk>/', CategoryInfoView.as_view(), name='category_detail'),
]
