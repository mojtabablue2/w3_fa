from rest_framework import serializers
from .models import Lesson, Category, Like, Comment, UserLessonClass, UserCategoryClass


class LikedObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `tagged_object` generic relationship.
    """

    def to_representation(self, value):
        """
        Serialize tagged objects to a simple textual representation.
        """
        # print(value.__class__.__name__)
        if isinstance(value, Lesson):
            return 'Lesson_likes: ' + value.get_num_likes()
        elif isinstance(value, Category):
            return 'Category_likes: ' + value.get_num_likes()
        raise Exception('Unexpected type of tagged object')


class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = [
            'object_id',
            'user',
            'content_type',
        ]


class CommentSerializer(serializers.ModelSerializer):
    likes = LikeSerializer(many=True)

    class Meta:
        model = Comment
        fields = '__all__'


class CommentingSerializer(serializers.ModelSerializer):
    text = serializers.CharField(max_length=200)

    class Meta:
        model = Comment
        fields = ['text']


class LessonSerializer(serializers.ModelSerializer):
    likes = LikeSerializer(many=True)
    comment = CommentSerializer(many=True)

    class Meta:
        model = Lesson
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    likes = LikeSerializer(many=True)
    comment = CommentSerializer(many=True)
    lesson = LessonSerializer()

    class Meta:
        model = Category
        fields = '__all__'


def grade_validate(value):
    if not 0 <= value <= 20:
        raise serializers.ValidationError('Not a valid grade')


# class UserEnrolledListSerializer(serializers.Serializer):
#     lesson = serializers.PrimaryKeyRelatedField(queryset=Lesson.objects.all())
#     grade = serializers.IntegerField(validators=[grade_validate])
#     join_at = serializers.DateTimeField()


# class LessonNameListSerializer(serializers.Serializer):
#     lesson = LessonSerializer(many=True)
#     grade = serializers.IntegerField(validators=[grade_validate])
#     join_at = serializers.DateTimeField()


class LessonNameListSerializer(serializers.ModelSerializer):
    lesson = serializers.StringRelatedField()

    class Meta:
        model = UserLessonClass
        fields = ['lesson', 'grade', 'join_at']


class CategoryComplete(serializers.ModelSerializer):
    complete_category = serializers.BooleanField()

    class Meta:
        model = UserCategoryClass
        fields = ['complete_category']
