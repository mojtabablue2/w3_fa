from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from user.models import Profile, CustomUser


class Like(models.Model):
    user = models.ForeignKey(CustomUser, related_name='likes', on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    like_object = GenericForeignKey('content_type', 'object_id')

    def get_num_likes(self):
        return self.objects.count()

    def __str__(self):
        return self.user.email + ' -> ' + self.content_type.name + \
               ' ( ' + Lesson.objects.get(id=self.object_id).title + ' )'


class Comment(models.Model):
    text = models.TextField(max_length=200)
    likes = GenericRelation(Like)
    num_like = models.PositiveIntegerField(default=0)
    author = models.ForeignKey(CustomUser,
                               on_delete=models.CASCADE,
                               related_name='comment'
                               )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    like_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        ordering = ('created',)

    def __str__(self):
        if self.content_type.name == 'category':
            return f'Comment by {self.author.email} on ({Category.objects.get(id=self.object_id).lesson.title} | ' \
                   f'{Category.objects.get(id=self.object_id).title})'
        if self.content_type.name == 'lesson':
            return f'Comment by {self.author.email} on ({Lesson.objects.get(id=self.object_id).title})'


class Lesson(models.Model):
    title = models.CharField(
        max_length=50,
        verbose_name=_("title")
    )
    slug = models.SlugField(
        max_length=100,
        null=True,
        blank=True,
        allow_unicode=True,
        unique=True,
        verbose_name=_("slug"),
    )
    description = RichTextField(
        null=True,
        blank=True,
    )
    image = models.ImageField(
        upload_to='images',
        default='default.jpg',
        verbose_name=_("image"),
    )
    publish_at = models.DateTimeField(
        default=timezone.now,
        verbose_name=_("publish_at"),
    )
    created = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    view_count = models.PositiveIntegerField(default=0)
    likes = GenericRelation(Like)
    comment = GenericRelation(Comment)
    user = models.ManyToManyField(CustomUser, related_name='lesson', through='UserLessonClass')

    num_like = models.PositiveIntegerField(default=0)

    def __str__(self):
        return f"{self.id}: {self.title}"

    class Meta:
        ordering = ['-created', 'title']

    def generate_slug(self):
        if self.title:
            return slugify(f"{self.title}", allow_unicode=True)
        return ""

    def save(self, *args, **kwargs):
        self.slug = self.generate_slug()
        super(Lesson, self).save(*args, **kwargs)

    def get_num_likes(self):
        return self.likes.count()


class Category(models.Model):
    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.CASCADE,
        related_name='category'
    )
    title = models.CharField(
        max_length=50,
        verbose_name=_("title"),
    )
    slug = models.SlugField(
        max_length=100,
        null=True,
        blank=True,
        unique=True,
        allow_unicode=True,
        verbose_name=_("slug"),
    )
    description = RichTextField(
        null=True,
        blank=True,
    )
    user = models.ManyToManyField(CustomUser, related_name='category', through='UserCategoryClass')
    view_count = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    likes = GenericRelation(Like)
    comment = GenericRelation(Comment)

    num_like = models.PositiveIntegerField(default=0)

    def __str__(self):
        return f'{self.id}: {self.title} ({self.lesson.title})'

    class Meta:
        ordering = ['-created', 'title']
        verbose_name_plural = 'Categories'

    def generate_slug(self):
        if self.title:
            return slugify(f"{self.lesson.title}_{self.title}", allow_unicode=True)
        return ""

    def get_num_likes(self):
        return self.likes.count()

    def save(self, *args, **kwargs):
        self.slug = self.generate_slug()
        super(Category, self).save(*args, **kwargs)


def validate_percentage(value):
    if 0 <= value <= 100:
        raise ValidationError(
            _(f'{value} is not an valid percent'),
            params={'value': value},
        )


class UserLessonClass(models.Model):
    user = models.ForeignKey(
        CustomUser,
        related_name='lesson_class',
        on_delete=models.SET_NULL,
        null=True)
    lesson = models.ForeignKey(
        Lesson,
        related_name='lesson_class',
        on_delete=models.SET_NULL,
        null=True)
    join_at = models.DateTimeField(verbose_name=_('Join date'), auto_now_add=True)
    grade = models.PositiveIntegerField(verbose_name=_('Grade'), default=0, null=True, blank=True)
    complete_percentage = models.PositiveIntegerField(verbose_name=_('Complete percent'),
                                                      default=0,
                                                      validators=[validate_percentage])

    def __str__(self):
        return f'{self.user.email} joins to {self.lesson.title}'

    class Meta:
        verbose_name_plural = 'User Lesson Classes'


class UserCategoryClass(models.Model):
    user = models.ForeignKey(
        CustomUser,
        related_name='category_class',
        on_delete=models.SET_NULL,
        null=True)
    category = models.ForeignKey(
        Category,
        related_name='category_class',
        on_delete=models.SET_NULL,
        null=True)
    complete_category = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user.email} joins to {self.category.title}'

    class Meta:
        verbose_name_plural = 'User Category Classes'
