from rest_framework.permissions import BasePermission


class IsUserOrAdmin(BasePermission):
    # def has_permission(self, request, view):
    #     print(request.user.id)
    #     return request.user and request.user.is_superuser
    message = 'you are not admin or owner of this profile'

    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser or obj.id == request.user.id