from django.contrib.auth import authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.urls import reverse
from requests import HTTPError
from social_core.backends.oauth import BaseOAuth2
from social_core.exceptions import MissingBackend, AuthTokenError, AuthForbidden
from social_django.utils import load_strategy, load_backend

from . import serializers
from .tasks import send_email

from rest_framework import status
from rest_framework.generics import (RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView,
                                     ListCreateAPIView, UpdateAPIView, GenericAPIView)
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
import jwt

from user.models import CustomUser, Profile
from user.permisions import IsUserOrAdmin
from user.serializers import (ProfileSerializer, SignupSerializer,
                              UserSerializer, ChangePasswordSerializer,
                              EmailSerializer, ResetPasswordSerializer)


class ProfileInfoView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, IsUserOrAdmin)
    serializer_class = ProfileSerializer

    def get_object(self):
        user = get_object_or_404(CustomUser, id=self.kwargs.get('pk', 0))

        self.check_object_permissions(self.request, user)

        profile = get_object_or_404(Profile, user=user)
        return profile


class SignupView(APIView):
    def post(self, request, format=None):
        serializer = SignupSerializer(data=request.data)
        if serializer.is_valid():
            # user = serializer.save()
            user = CustomUser.objects.create_user(serializer.validated_data['email'],
                                                  serializer.validated_data['password'], )
            user.first_name = serializer.validated_data['first_name']
            user.last_name = serializer.validated_data['last_name']
            user.save()

            send_verification_email(request, user)
            return Response('Please confirm your email address to complete the signup',
                            status=status.HTTP_201_CREATED)
            # return Response(UserSerializer(user).data, status=status.HTTP_201_CREATED)
        else:
            return Response({'message': 'validation failed',
                             'errors': serializer.errors}, status=status.HTTP_404_NOT_FOUND)


def get_jwt_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)

    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class LoginView(APIView):
    def post(self, request, format=None):
        data = request.data
        email = data.get('email', None)
        password = data.get('password', None)
        user = authenticate(email=email, password=password)
        if user:
            if user.is_staff:
                jwt_token = get_jwt_tokens_for_user(user)
                return Response(jwt_token, status=status.HTTP_200_OK)
            else:
                send_verification_email(request, user)
                return Response('This user is not active, so we send you verification mail to your email address',
                                status=status.HTTP_404_NOT_FOUND)
        else:
            return Response('User not found!', status=status.HTTP_404_NOT_FOUND)


class VerifyEmailView(APIView):
    def get(self, request, format=None):
        token = request.GET.get('token')
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user = CustomUser.objects.get(id=payload['user_id'])
            if not user.is_staff:
                user.is_staff = True
                user.save()
                return Response({'email': 'Successfully activated'}, status=status.HTTP_200_OK)
            else:
                return Response({'email': 'Already activated'}, status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError:
            return Response({'email': 'Activation link expired'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError:
            return Response({'email': 'Invalid token!'}, status=status.HTTP_400_BAD_REQUEST)
        except CustomUser.DoesNotExist:
            return Response({'email': 'User not found'}, status=status.HTTP_404_NOT_FOUND)


def send_verification_email(request, user):
    current_site = get_current_site(request).domain
    token = RefreshToken.for_user(user)
    relative_link = reverse('email_verify')
    abs_url = 'http://' + current_site + relative_link + '?token=' + str(token.access_token)
    email_body = 'Hi ' + user.first_name + ' ' + user.last_name + \
                 '\nuse link below to verify your email\n' + abs_url
    email_data = {'email_subject': 'Activate your W3_fa account',
                  'email_body': email_body,
                  'email_to': user.email}
    send_email.delay(email_data)


class PasswordChangeView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer

    def get_object(self, queryset=None):
        return self.request.user

    def update(self, request, *args, **kwargs):
        user = self.get_object()
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid():
            if not user.check_password(serializer.data.get("old_password")):
                return Response({'old_password': 'your entered password is not correct'},
                                status=status.HTTP_400_BAD_REQUEST)
            user.set_password(serializer.data.get('new_password'))
            user.save()
            return Response({'Your password updated successfully'}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordRequest(APIView):
    serializer_class = EmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data['email']
            try:
                user = CustomUser.objects.get(email=email)

            except CustomUser.DoesNotExist:
                return Response({'User not found with this entered email address'}, status=status.HTTP_404_NOT_FOUND)

            if not user.is_staff:
                return Response({'This user is not active'}, status=status.HTTP_400_BAD_REQUEST)
            current_site = get_current_site(request).domain
            token = RefreshToken.for_user(user)
            relative_link = reverse('password_reset_confirm')
            abs_url = 'http://' + current_site + relative_link + '?token=' + str(token.access_token)
            email_body = 'Hi ' + user.first_name + ' ' + user.last_name + \
                         '\nuse link below to reset your password\n' + abs_url
            email_data = {'email_subject': 'Password reset your W3_fa account',
                          'email_body': email_body,
                          'email_to': user.email}
            send_email.delay(email_data)
            return Response('We send you password reset link to your email address',
                            status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordValidateToken(APIView):
    serializer_class = ResetPasswordSerializer

    def post(self, request, format=None):
        token = request.GET.get('token')
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user = CustomUser.objects.get(id=payload['user_id'])
            serializer = ResetPasswordSerializer(data=request.data)
            if serializer.is_valid():
                user.set_password(serializer.data.get('new_password'))
                user.save()
                return Response({'Your password reset successfully'}, status=status.HTTP_200_OK)

            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except jwt.ExpiredSignatureError:
            return Response({'email': 'Reset link expired'}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError:
            return Response({'email': 'Invalid token!'}, status=status.HTTP_400_BAD_REQUEST)
        except CustomUser.DoesNotExist:
            return Response({'email': 'User not found'}, status=status.HTTP_404_NOT_FOUND)

# class SocialLoginView(GenericAPIView):
#     """Log in using facebook"""
#     serializer_class = serializers.SocialSerializer
#     permission_classes = [AllowAny]
#
#     def post(self, request):
#         """Authenticate user through the provider and access_token"""
#         serializer = self.serializer_class(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         provider = serializer.data.get('provider', None)
#         strategy = load_strategy(request)
#
#         try:
#             backend = load_backend(strategy=strategy, name=provider,
#                                    redirect_uri=None)
#
#         except MissingBackend:
#             return Response({'error': 'Please provide a valid provider'},
#                             status=status.HTTP_400_BAD_REQUEST)
#         try:
#             if isinstance(backend, BaseOAuth2):
#                 access_token = serializer.data.get('access_token')
#             user = backend.do_auth(access_token)
#         except HTTPError as error:
#             return Response({
#                 "error": {
#                     "access_token": "Invalid token",
#                     "details": str(error)
#                 }
#             }, status=status.HTTP_400_BAD_REQUEST)
#         except AuthTokenError as error:
#             return Response({
#                 "error": "Invalid credentials",
#                 "details": str(error)
#             }, status=status.HTTP_400_BAD_REQUEST)
#
#         try:
#             authenticated_user = backend.do_auth(access_token, user=user)
#
#         except HTTPError as error:
#             return Response({
#                 "error": "invalid token",
#                 "details": str(error)
#             }, status=status.HTTP_400_BAD_REQUEST)
#
#         except AuthForbidden as error:
#             return Response({
#                 "error": "invalid token",
#                 "details": str(error)
#             }, status=status.HTTP_400_BAD_REQUEST)
#
#         if authenticated_user and authenticated_user.is_active:
#             # generate JWT token
#             login(request, authenticated_user)
#             data = {
#                 "token": get_jwt_tokens_for_user(user)
#             }
#             # customize the response to your needs
#             response = {
#                 "email": authenticated_user.email,
#                 "username": authenticated_user.username,
#                 "token": data.get('token')
#             }
#             return Response(status=status.HTTP_200_OK, data=response)
