import datetime

from django.db import models
from django.contrib.auth.models import (PermissionsMixin,
                                        AbstractBaseUser,
                                        Group,
                                        Permission)
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _


class CustomUserManger(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password=None, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        user = self._create_user(
            email=email,
            password=password,
            **extra_fields,
        )
        user.is_staff = False
        user.is_superuser = False
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):

        user = self._create_user(
            email=email,
            password=password,
            **extra_fields,
        )
        user.is_staff = True
        user.is_superuser = True

        if user.is_staff is not True:
            raise ValueError('Superuser must have assign to is_staff=True.')
        if user.is_superuser is not True:
            raise ValueError('Superuser must have assign to is_superuser=True.')
        if user.is_active is not True:
            raise ValueError('Superuser must have assign to is_active=True.')
        user.save()
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('user permissions'),
        blank=True,
        help_text=_('Specific permissions for this user.'),
        related_name="user_sets",
        related_query_name="user",
    )
    groups = models.ManyToManyField(
        Group,
        blank=True,
    )
    first_name = models.CharField(
        max_length=20,
        blank=True,
        null=True,
    )
    last_name = models.CharField(
        max_length=20,
        blank=True,
        null=True,
    )
    password = models.CharField(
        _('password'),
        max_length=165,
        null=True,
        blank=True
    )

    email = models.CharField(
        verbose_name=_('Email Address'),
        help_text='Enter a valid email address',
        max_length=40,
        unique=True,
        null=True,
        blank=True,
    )
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    joined_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'User'

    USERNAME_FIELD = 'email'

    objects = CustomUserManger()

    def get_short_name(self):
        return self.first_name

    def get_full_name(self):
        return self.first_name + " " + self.last_name

    def __str__(self):
        if (self.first_name and self.last_name) is not None:
            return self.get_full_name()
        elif self.first_name is not None:
            return self.get_short_name()
        elif self.email is not None:
            return self.email


def user_profile_image_path(instance, filename):
    now = datetime.datetime.now()
    return 'users/profile_image/{0}/{1}/{2}/user_{3}/{4}'.format(
        now.strftime("%Y"),
        now.strftime("%m"),
        now.strftime("%d"),
        instance.user.id,
        filename)


class Profile(models.Model):
    user = models.OneToOneField(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='profiles'
    )
    born_date = models.DateField(
        null=True,
        blank=True,
    )
    profile_image = models.ImageField(
        verbose_name=_("Profile Image"),
        upload_to=user_profile_image_path,
        blank=True,
        null=True,
    )
    user_info = models.TextField(
        verbose_name=_("More Info"),
        blank=True,
        null=True,
    )

    complete_percentage = models.PositiveSmallIntegerField(_("percentage of completion"), default=0)

    def set_percent(self):
        percent = 0
        if self.user.email:
            percent += 23

        if self.profile_image:
            percent += 23

        if self.user.first_name and self.user.last_name:
            percent += 26

        if self.born_date:
            percent += 23

        Profile.objects.filter(id=self.id).update(complete_percentage=percent)

    def __str__(self):
        return self.user.__str__()

