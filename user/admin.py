from django.contrib import admin

from .forms import UserCreationForm
from .models import CustomUser, Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user',
                    'born_date',
                    'complete_percentage',
                    ]
    search_fields = ['first_name',
                     'last_name',
                     "email",
                     ]
    list_filter = ['born_date']
    autocomplete_fields = ['user']


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    form = UserCreationForm

    list_display = ["email",
                    "first_name",
                    "last_name",
                    "joined_at",
                    ]
    list_filter = ['joined_at',
                   'first_name',
                   'last_name',
                   ]
    fieldsets = (
        (None, {'fields': ('email',
                           'password',
                           'user_permissions',
                           'groups',
                           'first_name',
                           'last_name',)}),
        ('Permissions', {'fields': ('is_staff',
                                    'is_active',
                                    'is_superuser',)}),
    )
    search_fields = ['first_name',
                     'last_name',
                     "email",
                     ]
    ordering = ('email',)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password'),
        }),
    )
