from celery import shared_task
from django.core.mail import EmailMessage

from W3_fa.celery import app


@shared_task
def send_email(email_data):
    email = EmailMessage(subject=email_data['email_subject'],
                         to=[email_data['email_to']],
                         body=email_data['email_body'])
    return email.send()

