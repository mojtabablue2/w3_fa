from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from user.models import CustomUser, Profile


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        exclude = ['password', 'user_permissions']


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Profile
        fields = '__all__'


class SignupSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(
        max_length=60,
        required=True,
    )
    last_name = serializers.CharField(
        max_length=60,
    )
    email = serializers.EmailField(
        max_length=100,
        required=True,
        validators=[UniqueValidator(queryset=CustomUser.objects.all())],
    )
    password = serializers.CharField(
        max_length=20,
        required=True,
    )

    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name', 'email', 'password']
        # extra_kwargs = {
        #     'password': {'write_only': True},
        # }

    def create(self, validated_data):
        user = CustomUser.objects.create_user(validated_data['email'],
                                              password=validated_data['password'], )
        return user


class ChangePasswordSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    class Meta:
        model = CustomUser
        fields = ['old_password', 'new_password']
        # fields = '__all__'


class ResetPasswordSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(required=True)

    class Meta:
        model = CustomUser
        fields = ['new_password']
        # fields = '__all__'


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

# class SocialSerializer(serializers.Serializer):
#     """
#     Serializer which accepts an OAuth2 access token and provider.
#     """
#     provider = serializers.CharField(max_length=255, required=True)
#     access_token = serializers.CharField(max_length=4096, required=True, trim_whitespace=True)
