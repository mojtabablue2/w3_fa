from django.db.models.signals import post_save
from .models import CustomUser, Profile
from django.dispatch import receiver


@receiver(post_save, sender=CustomUser)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


# @receiver(post_save, sender=CustomUser)
# def save_profile(sender, instance, **kwargs):
#     instance.profiles.save()


@receiver(post_save, sender=Profile)
def update_percent_complete_profile(sender, instance, **kwargs):
    instance.complete_percentage = instance.set_percent()
