from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views
# from .views import SocialLoginView

urlpatterns = [
    path('<int:pk>/profile/', views.ProfileInfoView.as_view()),
    path('signup/', views.SignupView.as_view(), name='signup'),
    path('signup_verify/', views.VerifyEmailView.as_view(), name='email_verify'),
    path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('password_reset/', views.ResetPasswordRequest.as_view(), name='password_reset'),
    path('password_reset/confirm/', views.ResetPasswordValidateToken.as_view(), name='password_reset_confirm'),
    path('login/', views.LoginView.as_view(), name='login'),
    # path('oauth/login/', SocialLoginView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
